Issues
======
### MySQL "Incorrect string value" error
* Alter character encoding for database  

    ```sql
    alter database db_name character set utf8
    ```  
* Or alter character encoding for table 

    ```sql
    alter table tbl_name convert to character set utf8
    ```
