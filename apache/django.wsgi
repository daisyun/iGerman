import os
import sys

path = '/var/www/igerman'
if path not in sys.path:
    sys.path.insert(0, '/var/www/igerman')

os.environ['DJANGO_SETTINGS_MODULE'] = 'igerman.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
