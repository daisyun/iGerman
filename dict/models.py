from django.db import models

# Create your models here.
class Word(models.Model):
    name = models.CharField(max_length=100)
    duden_link = models.CharField(max_length=100)
    audio_link = models.CharField(max_length=100)
    betonung = models.CharField(max_length=100)
    worttrennung = models.CharField(max_length=100)    

    def __unicode__(self):
        return self.name
    
